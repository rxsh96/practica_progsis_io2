CC=gcc
CFLAGS=-c -I.
OBJECT=check_sha1.o csapp.o

all: check_sha1

check_sha1: $(OBJECT)
	$(CC) -static -o check_sha1 $(OBJECT) -I. -L. -lsha1

check_sha1.o: check_sha1.c 
	$(CC) $(CFLAGS) check_sha1.c

csapp.o: csapp.c csapp.h
	$(CC) $(CFLAGS) csapp.c

clean:
	rm -f *.o
	rm -f check_sha1

# Pr�ctica 10: Uso de llamadas I/O en Linux #

Este es un repositorio esqueleto para la pr�ctica 10 de la materia Programaci�n de Sistemas (CCPG1008) P1 de la ESPOL.

### �C�mo empiezo? ###

* Hacer un fork de este repositorio a su cuenta personal de Bitbucket (una cuenta por grupo)
* Clonar el repositorio en su cuenta (no este) en su computadora del laboratorio
* Completar la pr�ctica en grupo
* Haga commit y push a su trabajo
* El entregable es un enlace al repositorio

### Integrantes ###


* Johnny Beltran
* Ricardo Serrano

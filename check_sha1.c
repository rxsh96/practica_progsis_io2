#include "csapp.h"
#include "libsha1.h"
#include <arpa/inet.h>
static void print_hex(const char* data, size_t size);
static char *get_hex(const char* data, size_t size);
int main(int argc, char **argv){
	
	struct stat mi_stat;
	int f_descriptorR;
	int f_descriptorR2;
	int f_descriptorW;
	
	
	if (argc == 2){

		/* Ejemplo como verificar existencia y tamaño de un archivo */
		if(stat(argv[1], &mi_stat) < 0){
			fprintf(stderr, "Archivo %s no existe!\n", argv[1]);
		}
		else{
			
			int st = mi_stat.st_size;
			char text[st];
			
			printf("El archivo %s tiene %d bytes!\n", argv[1], st);
			
			//TODO: Leer archivo usando rio_readn
			f_descriptorR = Open(argv[1], O_RDONLY, 0);
			rio_readn(f_descriptorR, text, st);
			
			//TODO: Calcular SHA1
			sha1_ctx ctx; 
			char dgst[SHA1_DIGEST_SIZE]; 
			sha1_begin(&ctx);
			for(int i = 0; i < st-1; ++i){
				sha1_hash(text+i, 1, &ctx);
			}
			sha1_end(dgst, &ctx);			
			
			//TODO: Mostrar SHA1 en consola en formato hexadecimal
			print_hex(dgst, SHA1_DIGEST_SIZE);
			printf("\n");
			
			//TODO: Escribir SHA1 en archivo <nombre_archivo>.sha1 usando rio_writen o fprintf
			char archivoW[20];
			strcpy(archivoW, argv[1]);
			strcat(archivoW,".sha1");
			char *datosHex = get_hex(dgst, SHA1_DIGEST_SIZE);
			f_descriptorW = Open(archivoW,O_WRONLY|O_CREAT, 0666);
			
			rio_writen(f_descriptorW, datosHex, SHA1_DIGEST_SIZE*2);
			
			
			Close(f_descriptorR);
			Close(f_descriptorW);
		}
	}
	else if (argc == 3){
		
		//TODO: Leer archivo usando rio_readn
		struct stat mi_stat2;
		if(stat(argv[1], &mi_stat) < 0 | stat(argv[2], &mi_stat2) < 0){
			fprintf(stderr, "Error. Revise el nombre de los archivos!\n");
		}else{
			int st = mi_stat.st_size;
			int st2 = mi_stat2.st_size;
			char text[st];
			char text2[st2];
			f_descriptorR = Open(argv[1], O_RDONLY, 0);
			rio_readn(f_descriptorR, text, st);
		
			//TODO: Calcular SHA1
			sha1_ctx ctx; 
			char dgst[SHA1_DIGEST_SIZE]; 
			sha1_begin(&ctx);
			for(int i = 0; i < st-1; ++i){
				sha1_hash(text+i, 1, &ctx);
			}
			sha1_end(dgst, &ctx);
			//TODO: Leer archivo con SHA1 usando rio_readn
			f_descriptorR2 = Open(argv[2], O_RDONLY, 0);
			rio_readn(f_descriptorR2, text2, st2);
			//TODO: Comparar ambos SHA1
			char *hex = get_hex(dgst, SHA1_DIGEST_SIZE);
			int compare = strcmp(hex, text2);
			//TODO: Mostrar resultado comparación
			if(compare == 0){
				printf("%s: OK!\n", argv[1]);
			}else{
				fprintf(stderr, "Corrupted file!\n");
			}
		}
	}
	else{
		fprintf(stderr, "uso: %s <nombre_archivo> [<nombre_archivo_sha1>]\n", argv[0]);
	}
}

static void print_hex(const char* data, size_t size){
    int i;
    printf("SHA1: ");
    for(i = 0; i < size; ++i){
        printf("%x",(unsigned char) data[i]);
	}
}
static char *get_hex(const char* data, size_t size){
	char *hex = calloc(1,size*2);
	uint32_t host;
	for(int i = 0; i < 36; i+=4){
		host = htonl(*(uint32_t *)&data[i/2]);
		sprintf(&hex[i], "%x", host);
	}
	return hex;
}


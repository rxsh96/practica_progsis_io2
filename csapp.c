#include "csapp.h"

int Open(const char *pathname, int flags, mode_t mode){
	
    int rc;

    if ((rc = open(pathname, flags, mode))  < 0){
		unix_error("Open error");
	}
    return rc;
    
}

void Close(int fd){
	
    int rc;

    if ((rc = close(fd)) < 0){
		unix_error("Close error");
	}
	
}

ssize_t rio_readn(int fd, void *usrbuf, size_t n){
	
    size_t nleft = n;
    ssize_t nread;
    char *bufp = usrbuf;

    while (nleft > 0) {
		if ((nread = read(fd, bufp, nleft)) < 0) {
			if (errno == EINTR) /* Interrupted by sig handler return */
			nread = 0;      /* and call read() again */
			else
			return -1;      /* errno set by read() */ 
		} 
		else if (nread == 0){
			break;              /* EOF */
		}
		nleft -= nread;
		bufp += nread;
    }
    return (n - nleft);         /* return >= 0 */
}

ssize_t rio_writen(int fd, void *usrbuf, size_t n){
	
    size_t nleft = n;
    ssize_t nwritten;
    char *bufp = usrbuf;

    while (nleft > 0) {
		if ((nwritten = write(fd, bufp, nleft)) <= 0) {
			if (errno == EINTR)  /* Interrupted by sig handler return */
			nwritten = 0;    /* and call write() again */
			else
			return -1;       /* errno set by write() */
		}
		nleft -= nwritten;
		bufp += nwritten;
	}
    return n;
    
}

void unix_error(char *msg){
	
    fprintf(stderr, "%s: %s\n", msg, strerror(errno));
    exit(0);
    
}


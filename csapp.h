#include <sys/stat.h>
#include <sys/types.h>
#include <unistd.h>
#include <string.h>
#include <errno.h>
#include <stdio.h>
#include <fcntl.h>
#include <stdlib.h>


int Open(const char *, int, mode_t);
void Close(int fd);
ssize_t rio_readn(int, void *, size_t);
ssize_t rio_writen(int, void *, size_t);
void unix_error(char *);

